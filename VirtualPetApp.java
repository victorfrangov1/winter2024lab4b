import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Horse[] stableOfHorses = new Horse[3];
		
		for(int i = 0; i < stableOfHorses.length; i++){
			System.out.println("What is the breed of your Horse: ");
			String breed = reader.nextLine();
			System.out.println("What is your Horse's weight: ");
			int weight = Integer.parseInt(reader.nextLine());
			System.out.println("How FAST is your Horse: ");
			int speed = Integer.parseInt(reader.nextLine());
			
			stableOfHorses[i] = new Horse(breed, weight, speed);
		}
		
		//For Q.13
		System.out.println("Fields of the last animal before set method: ");
		System.out.println(stableOfHorses[stableOfHorses.length-1].getBreed());
		System.out.println(stableOfHorses[stableOfHorses.length-1].getWeight());
		System.out.println(stableOfHorses[stableOfHorses.length-1].getSpeed());
		
		System.out.println("Modify your horse's speed to : ");
		stableOfHorses[stableOfHorses.length-1].setSpeed(Integer.parseInt(reader.nextLine()));
		
		//For Q.13
		System.out.println("Fields of the last animal after set method: ");
		System.out.println(stableOfHorses[stableOfHorses.length-1].getBreed());
		System.out.println(stableOfHorses[stableOfHorses.length-1].getWeight());
		System.out.println(stableOfHorses[stableOfHorses.length-1].getSpeed());
	}
}